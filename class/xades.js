const os = require('os');
const fs = require('fs');
const forge = require('node-forge');
const pem = require('pem');
const path = require('path');

// Change here for specify the source for Cert File and Password File
const CERT_FILE = __dirname + '/../cert/cert.pfx';
const CERT_PASSWORD_FILE = __dirname + '/../cert/password_cert.txt';

console.log(os.platform());
if (os.platform() === 'win32') {
  pem.config({
    pathOpenSSL: path.join(__dirname + './../', 'openssl', 'windows', 'openssl.exe')
  });
}

module.exports = class XadesClass {

  constructor() {
      this.xadesjs = require("xadesjs");
      this.xmldsigjs = require("xmldsigjs");
      this.webcrypto = require('node-webcrypto-ossl');
      this.xadesjs.Application.setEngine("OpenSSL", new this.webcrypto());
      this.xmldsigjs.Application.setEngine("OpenSSL", new this.webcrypto());
      this.signedXML = '';
  }

  preparePem(pemString) {
    return pemString
      // remove BEGIN/END
      .replace(/-----(BEGIN|END)[\w\d\s]+-----/g, "")
      // remove \r, \n
      .replace(/[\r\n]/g, "");
  }

  readPasswordCertificate() {
    return fs.readFileSync(CERT_PASSWORD_FILE).toString();
  }

  readCertificate() {
    var self = this;
    return new Promise(
      function (resolve, reject) {
        var pfx = fs.readFileSync(CERT_FILE);
        pem.readPkcs12(pfx, { p12Password: self.readPasswordCertificate()}, (error, certificate) => {
          if (error) {
            reject(error);
          } else {
            self.PEMObject = certificate;
            self.pkiPrivateKey = forge.pki.privateKeyFromPem(certificate.key);
            self.pkiPublicKey = forge.pki.setRsaPublicKey(self.pkiPrivateKey.n, self.pkiPrivateKey.e);
            resolve();
          }
        });
      });
  }

  async importKeysFromCertificate() {
    await this.readCertificate();
    const pemPublicKey = forge.pki.publicKeyToPem(this.pkiPublicKey);
    const crypto = new this.webcrypto();
    // const hash = "SHA-256";
    const hash = "SHA-1";
    const algorithm = {
        name: "RSASSA-PKCS1-v1_5",
        modulusLength: 2048,
        publicExponent: new Uint8Array([1, 0, 1]),
        hash,
    };
    const privateKeyDER = this.pem2der(this.PEMObject.key);
    const cryptoPrivateKey = await crypto.subtle.importKey("pkcs8", privateKeyDER, algorithm, false, ["sign", "verify"]);
    const publicKeyDER = this.pem2der(pemPublicKey);
    const cryptoPublicKey = await crypto.subtle.importKey("spki", publicKeyDER, algorithm, true, ["sign", "verify"]);
    const x509 = this.preparePem(this.PEMObject.cert);
    const cert = this.pem2der(this.PEMObject.cert);
    return {
      privateKey: cryptoPrivateKey,
      publicKey: cryptoPublicKey,
      certDER: cert,
      x509: x509,
    }
  }
  
  pem2der(pemString) {
    pemString = this.preparePem(pemString);
    // convert base64 to ArrayBuffer
    return new Uint8Array(Buffer.from(pemString, "base64")).buffer;
  }

  signWithRandom(xmlString) {
      let self = this;

      return self.xadesjs.Application.crypto.subtle.generateKey({
          name: "RSASSA-PKCS1-v1_5",
          modulusLength: 2048, //can be 1024, 2048, or 4096,
          publicExponent: new Uint8Array([1, 0, 1]),
          hash: { name: "SHA-1" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
      },
      false, //whether the key is extractable (i.e. can be used in exportKey)
      ["sign", "verify"] //can be any combination of "sign" and "verify"
      )
        .then(function (keyPair) {
            // Call sign function
            return self.signXml(xmlString, keyPair, { name: "RSASSA-PKCS1-v1_5", hash: { name: "SHA-1" } });
        })
        .then(function (signedDocument) {
            self.signedXML = signedDocument;
        })
        .catch(function (e) {
            console.error(e);
        });
  }

  signWithCert(xmlString) {
    let self = this;
    return this.importKeysFromCertificate()
      .then((keyPair) => {
        // Call sign function
        return self.signXmlWithCert(xmlString, keyPair, { name: "RSASSA-PKCS1-v1_5", hash: { name: "SHA-1" } });
      })
      .then(function (signedDocument) {
        self.signedXML = signedDocument;
      })
      .catch(function (e) {
        console.error(e);
      });
  }

  signXml(xmlString, keys, algorithm) {
    let self = this;
    return Promise.resolve()
      .then(() => {
        var xmlDoc = self.xadesjs.Parse(xmlString);
        var signedXml = new self.xadesjs.SignedXml();

        return signedXml.Sign(  // Signing document
          algorithm,          // algorithm
          keys.privateKey,    // key
          xmlDoc,             // document
          {                   // options
            x509: [
              "MIIGgTCCBGmgAwIBAgIUeaHFHm5f58zYv20JfspVJ3hossYwDQYJKoZIhvcNAQEFBQAwgZIxCzAJBgNVBAYTAk5MMSAwHgYDVQQKExdRdW9WYWRpcyBUcnVzdGxpbmsgQi5WLjEoMCYGA1UECxMfSXNzdWluZyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTE3MDUGA1UEAxMuUXVvVmFkaXMgRVUgSXNzdWluZyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eSBHMjAeFw0xMzEwMzAxMjI3MTFaFw0xNjEwMzAxMjI3MTFaMHoxCzAJBgNVBAYTAkJFMRAwDgYDVQQIEwdCcnVzc2VsMRIwEAYDVQQHEwlFdHRlcmJlZWsxHDAaBgNVBAoTE0V1cm9wZWFuIENvbW1pc3Npb24xFDASBgNVBAsTC0luZm9ybWF0aWNzMREwDwYDVQQDDAhFQ19ESUdJVDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJgkkqvJmZaknQC7c6H6LEr3dGtQ5IfOB3HAZZxOZbb8tdM1KMTO3sAifJC5HNFeIWd0727uZj+V5kBrUv36zEs+VxiN1yJBmcJznX4J2TCyPfLk2NRELGu65VwrK2Whp8cLLANc+6pQn/5wKh23ehZm21mLXcicZ8whksUGb/h8p6NDe1cElD6veNc9CwwK2QT0G0mQiEYchqjJkqyY8HEak8t+CbIC4Rrhyxh3HI1fCK0WKS9JjbPQFbvGmfpBZuLPYZYzP4UXIqfBVYctyodcSAnSfmy6tySMqpVSRhjRn4KP0EfHlq7Ec+H3nwuqxd0M4vTJlZm+XwYJBzEFzFsCAwEAAaOCAeQwggHgMFgGA1UdIARRME8wCAYGBACLMAECMEMGCisGAQQBvlgBgxAwNTAzBggrBgEFBQcCARYnaHR0cDovL3d3dy5xdW92YWRpc2dsb2JhbC5ubC9kb2N1bWVudGVuMCQGCCsGAQUFBwEDBBgwFjAKBggrBgEFBQcLAjAIBgYEAI5GAQEwdAYIKwYBBQUHAQEEaDBmMCoGCCsGAQUFBzABhh5odHRwOi8vb2NzcC5xdW92YWRpc2dsb2JhbC5jb20wOAYIKwYBBQUHMAKGLGh0dHA6Ly90cnVzdC5xdW92YWRpc2dsb2JhbC5jb20vcXZldWNhZzIuY3J0MEYGCiqGSIb3LwEBCQEEODA2AgEBhjFodHRwOi8vdHNhMDEucXVvdmFkaXNnbG9iYWwuY29tL1RTUy9IdHRwVHNwU2VydmVyMBMGCiqGSIb3LwEBCQIEBTADAgEBMA4GA1UdDwEB/wQEAwIGQDAfBgNVHSMEGDAWgBTg+A751LXyf0kjtsN5x6M1H4Z6iDA7BgNVHR8ENDAyMDCgLqAshipodHRwOi8vY3JsLnF1b3ZhZGlzZ2xvYmFsLmNvbS9xdmV1Y2FnMi5jcmwwHQYDVR0OBBYEFDc3hgIFJTDamDEeQczI7Lot4uaVMA0GCSqGSIb3DQEBBQUAA4ICAQAZ8EZ48RgPimWY6s4LjZf0M2MfVJmNh06Jzmf6fzwYtDtQLKzIDk8ZtosqYpNNBoZIFICMZguGRAP3kuxWvwANmrb5HqyCzXThZVPJTmKEzZNhsDtKu1almYBszqX1UV7IgZp+jBZ7FyXzXrXyF1tzXQxHGobDV3AEE8vdzEZtwDGpZJPnEPCBzifdY+lrrL2rDBjbv0VeildgOP1SIlL7dh1O9f0T6T4ioS6uSdMt6b/OWjqHadsSpKry0A6pqfOqJWAhDiueqgVB7vus6o6sSmfG4SW9EWW+BEZ510HjlQU/JL3PPmf+Xs8s00sm77LJ/T/1hMUuGp6TtDsJe+pPBpCYvpm6xu9GL20CsArFWUeQ2MSnE1jsrb00UniCKslcM63pU7I0VcnWMJQSNY28OmnFESPK6s6zqoN0ZMLhwCVnahi6pouBwTb10M9/Anla9xOT42qxiLr14S2lHy18aLiBSQ4zJKNLqKvIrkjewSfW+00VLBYbPTmtrHpZUWiCGiRS2SviuEmPVbdWvsBUaq7OMLIfBD4nin1FlmYnaG9TVmWkwVYDsFmQepwPDqjPs4efAxzkgUFHWn0gQFbqxRocKrCsOvCDHOHORA97UWcThmgvr0Jl7ipvP4Px//tRp08blfy4GMzYls5WF8f6JaMrNGmpfPasd9NbpBNp7A=="
            ],
            keyValue: keys.publicKey,
            references: [
              { hash: "SHA-1", transforms: ["enveloped"] }
            ],
            signingCertificate: "MIIGgTCCBGmgAwIBAgIUeaHFHm5f58zYv20JfspVJ3hossYwDQYJKoZIhvcNAQEFBQAwgZIxCzAJBgNVBAYTAk5MMSAwHgYDVQQKExdRdW9WYWRpcyBUcnVzdGxpbmsgQi5WLjEoMCYGA1UECxMfSXNzdWluZyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTE3MDUGA1UEAxMuUXVvVmFkaXMgRVUgSXNzdWluZyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eSBHMjAeFw0xMzEwMzAxMjI3MTFaFw0xNjEwMzAxMjI3MTFaMHoxCzAJBgNVBAYTAkJFMRAwDgYDVQQIEwdCcnVzc2VsMRIwEAYDVQQHEwlFdHRlcmJlZWsxHDAaBgNVBAoTE0V1cm9wZWFuIENvbW1pc3Npb24xFDASBgNVBAsTC0luZm9ybWF0aWNzMREwDwYDVQQDDAhFQ19ESUdJVDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJgkkqvJmZaknQC7c6H6LEr3dGtQ5IfOB3HAZZxOZbb8tdM1KMTO3sAifJC5HNFeIWd0727uZj+V5kBrUv36zEs+VxiN1yJBmcJznX4J2TCyPfLk2NRELGu65VwrK2Whp8cLLANc+6pQn/5wKh23ehZm21mLXcicZ8whksUGb/h8p6NDe1cElD6veNc9CwwK2QT0G0mQiEYchqjJkqyY8HEak8t+CbIC4Rrhyxh3HI1fCK0WKS9JjbPQFbvGmfpBZuLPYZYzP4UXIqfBVYctyodcSAnSfmy6tySMqpVSRhjRn4KP0EfHlq7Ec+H3nwuqxd0M4vTJlZm+XwYJBzEFzFsCAwEAAaOCAeQwggHgMFgGA1UdIARRME8wCAYGBACLMAECMEMGCisGAQQBvlgBgxAwNTAzBggrBgEFBQcCARYnaHR0cDovL3d3dy5xdW92YWRpc2dsb2JhbC5ubC9kb2N1bWVudGVuMCQGCCsGAQUFBwEDBBgwFjAKBggrBgEFBQcLAjAIBgYEAI5GAQEwdAYIKwYBBQUHAQEEaDBmMCoGCCsGAQUFBzABhh5odHRwOi8vb2NzcC5xdW92YWRpc2dsb2JhbC5jb20wOAYIKwYBBQUHMAKGLGh0dHA6Ly90cnVzdC5xdW92YWRpc2dsb2JhbC5jb20vcXZldWNhZzIuY3J0MEYGCiqGSIb3LwEBCQEEODA2AgEBhjFodHRwOi8vdHNhMDEucXVvdmFkaXNnbG9iYWwuY29tL1RTUy9IdHRwVHNwU2VydmVyMBMGCiqGSIb3LwEBCQIEBTADAgEBMA4GA1UdDwEB/wQEAwIGQDAfBgNVHSMEGDAWgBTg+A751LXyf0kjtsN5x6M1H4Z6iDA7BgNVHR8ENDAyMDCgLqAshipodHRwOi8vY3JsLnF1b3ZhZGlzZ2xvYmFsLmNvbS9xdmV1Y2FnMi5jcmwwHQYDVR0OBBYEFDc3hgIFJTDamDEeQczI7Lot4uaVMA0GCSqGSIb3DQEBBQUAA4ICAQAZ8EZ48RgPimWY6s4LjZf0M2MfVJmNh06Jzmf6fzwYtDtQLKzIDk8ZtosqYpNNBoZIFICMZguGRAP3kuxWvwANmrb5HqyCzXThZVPJTmKEzZNhsDtKu1almYBszqX1UV7IgZp+jBZ7FyXzXrXyF1tzXQxHGobDV3AEE8vdzEZtwDGpZJPnEPCBzifdY+lrrL2rDBjbv0VeildgOP1SIlL7dh1O9f0T6T4ioS6uSdMt6b/OWjqHadsSpKry0A6pqfOqJWAhDiueqgVB7vus6o6sSmfG4SW9EWW+BEZ510HjlQU/JL3PPmf+Xs8s00sm77LJ/T/1hMUuGp6TtDsJe+pPBpCYvpm6xu9GL20CsArFWUeQ2MSnE1jsrb00UniCKslcM63pU7I0VcnWMJQSNY28OmnFESPK6s6zqoN0ZMLhwCVnahi6pouBwTb10M9/Anla9xOT42qxiLr14S2lHy18aLiBSQ4zJKNLqKvIrkjewSfW+00VLBYbPTmtrHpZUWiCGiRS2SviuEmPVbdWvsBUaq7OMLIfBD4nin1FlmYnaG9TVmWkwVYDsFmQepwPDqjPs4efAxzkgUFHWn0gQFbqxRocKrCsOvCDHOHORA97UWcThmgvr0Jl7ipvP4Px//tRp08blfy4GMzYls5WF8f6JaMrNGmpfPasd9NbpBNp7A=="
          })
      })
      .then(signature => signature.toString());
  }

  signXmlWithCert(xmlString, keys, algorithm) {
    let self = this;
    return Promise.resolve()
      .then(() => {
        var xmlDoc = self.xadesjs.Parse(xmlString);
        var signedXml = new self.xadesjs.SignedXml();

        /*
        // Especifiying Default prefix to etsi
        // Global Xades Class
        //this.xadesjs.xml.XmlXades. =
        this.xadesjs.xml.QualifyingProperties.prefix = 
        this.xadesjs.xml.SignedProperties.prefix = 
        this.xadesjs.xml.SignedSignatureProperties.prefix = 
        //this.xadesjs.xml.SignaturePolicyIdentifier.prefix =
        //this.xadesjs.xml.SignedDataObjectProperties.prefix =
        this.xadesjs.xml.XadesDateTime.prefix = 
        this.xadesjs.xml.SigningCertificate.prefix = 
        this.xadesjs.xml.Cert.prefix = 
        this.xadesjs.xml.DigestAlgAndValueType.prefix =
        this.xadesjs.xml.IssuerSerial.prefix =
        //this.xadesjs.xml.DataObjectFormat.prefix =
        //this.xadesjs.xml.DataObjectFormatCollection.prefix =
        // Main Nodes on Xades Signature
        signedXml.properties.prefix = 
        signedXml.properties.SignedProperties.prefix = 
         'etsi';
         //console.log(signedXml.properties);*/

         //Adding Custom KeyInfo node
        var keyInfoX509Data = new self.xmldsigjs.KeyInfoX509Data();
        const x509Certificate = new self.xmldsigjs.X509Certificate(keys.certDER);
        keyInfoX509Data.AddSubjectName(x509Certificate.Subject);
        keyInfoX509Data.AddCertificate(x509Certificate);
        keyInfoX509Data.AddIssuerSerial(x509Certificate.Issuer, x509Certificate.SerialNumber);

        signedXml.XmlSignature.KeyInfo.Id = this.getRandomGUID('KeyInfo');
        signedXml.XmlSignature.KeyInfo.Add(keyInfoX509Data);

        //Adding References
        const refURIKeyInfo = '#' + signedXml.XmlSignature.KeyInfo.Id;
        const refURIComprobante = '#comprobante';
        
        var mainXMLRef = new this.xmldsigjs.Reference(refURIComprobante);
        mainXMLRef.Id = this.getRandomGUID("Reference-Comprobante")
        mainXMLRef.DigestMethod.Algorithm = this.xmldsigjs.SHA1_NAMESPACE;
        mainXMLRef.Transforms.Add(new this.xmldsigjs.XmlDsigEnvelopedSignatureTransform());

        var keyInfoRef = new this.xmldsigjs.Reference(refURIKeyInfo);
        keyInfoRef.Id = this.getRandomGUID("Reference-KeyInfo");
        keyInfoRef.DigestMethod.Algorithm = this.xmldsigjs.SHA1_NAMESPACE;

        signedXml.XmlSignature.SignedInfo.References.Add(mainXMLRef);
        signedXml.XmlSignature.SignedInfo.References.Add(keyInfoRef);

        //Adding DataObject Properties
        var signedDataObjectProperties = new this.xadesjs.xml.SignedDataObjectProperties();
        var dataObjectFormat = new this.xadesjs.xml.DataObjectFormat();
        dataObjectFormat.ObjectReference = "#" + mainXMLRef.Id;
        dataObjectFormat.Description = "Contenido Comprobante";
        dataObjectFormat.MimeType = "text/xml";
        signedDataObjectProperties.DataObjectFormats.Add(dataObjectFormat);

        signedXml.properties.SignedProperties.SignedDataObjectProperties = signedDataObjectProperties;

        return signedXml.Sign(  // Signing document
          algorithm,          // algorithm
          keys.privateKey,    // key
          xmlDoc,             // document
          {                   // options
            keyValue: keys.publicKey,
            policy: null,
            signingCertificate: keys.x509
          })
      })
      .then(signature => {
        var xmlDoc = self.xadesjs.Parse(xmlString);
        xmlDoc.documentElement.appendChild(signature.GetXml());
        return xmlDoc.toString();
      });
  }

  getRandomGUID(prefix) {
    function _p8(s) {
      var p = (Math.random().toString(16) + "000000000").substr(2,8);

      return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
    }

    return prefix + "ID-" + _p8() + _p8(true) + _p8(true) + _p8();
  }
}