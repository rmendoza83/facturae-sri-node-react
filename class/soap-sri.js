const soap = require('strong-soap').soap;
const btoa = require('btoa');

const PRODUCTION_SERVER = 'https://cel.sri.gob.ec/';
const TEST_SERVER = 'https://celcer.sri.gob.ec/';

const SERVICE_RECEPCION = 'comprobantes-electronicos-ws/RecepcionComprobantesOffline?wsdl';
const SERVICE_AUTORIZACION = 'comprobantes-electronicos-ws/AutorizacionComprobantesOffline?wsdl';

module.exports = class SOAPClientSRI {

  constructor (productionServer) {
      if (productionServer) {
          this.recepcionURL = PRODUCTION_SERVER + SERVICE_RECEPCION;
          this.autorizacionURL = PRODUCTION_SERVER + SERVICE_AUTORIZACION;
      } else {
          this.recepcionURL = TEST_SERVER + SERVICE_RECEPCION;
          this.autorizacionURL = TEST_SERVER + SERVICE_AUTORIZACION;
      }
  }

  getBase64(str) {
    return btoa(unescape(encodeURIComponent(str)));
  }

  getBytesArray(str) {
    var myBuffer = [];
    var buffer = new Buffer(str, 'utf8');
    for (var i = 0; i < buffer.length; i++) {
        myBuffer.push(buffer[i]);
    }
    return myBuffer;
  }

  validarComprobante(xml) {
    var self = this;
    return new Promise((resolve, reject) => {
      soap.createClient(self.recepcionURL, {}, function(err, soapClient) {
        const validarComprobante = soapClient['validarComprobante'];
        const args = {
          'xml': self.getBase64(xml)
        };
        if (err) {
          reject(err);
        }
        validarComprobante(args, function(err, result, envelope, soapHeader) {
          resolve(result);
        });
      });
    }); 
  }

  autorizacionComprobante(claveAccesoComprobante) {
    var self = this;
    return new Promise((resolve, reject) => {
      soap.createClient(self.autorizacionURL, {}, function(err, soapClient) {
        const autorizacionComprobante = soapClient['autorizacionComprobante'];
        const args = {
          'claveAccesoComprobante': claveAccesoComprobante
        };
        if (err) {
          reject(err);
        }
        autorizacionComprobante(args, function(err, result, envelope, soapHeader) {
          resolve(result);
        });
      });
    }); 
  }
}