import React, { Component } from 'react';
import logo from './logo.svg';
import {
  SRIURL,
  SRIEmision,
  SRITipoComprobante,
  SRITipoAmbiente,
  SRITipoIndentificacion,
  SRIMoneda } from './class/sri-constant';
import {
  XMLFacturaDetalleAdicional,
  XMLFacturaDetalleImpuesto,
  XMLFacturaDetalle,
  XMLFacturaInfoTributaria,
  XMLFacturaPago,
  XMLFacturaTotalImpuesto,
  XMLFacturaInfoFactura,
  XMLFacturaInfoAdicional,
  XMLFactura,
  XMLFacturaXades, 
  SOAPClientSRI} from './class/xml-factura';
import './App.css';
import moment from 'moment';

class App extends Component {
  facturaXML;
  facturaXMLSign;
  soapClientSRI;

  constructor() {
    super();

    this.state = {
      xmlString: '',
      xmlSignedString: '',
      respuestaSRI: ''
    }
    this.getFactura();
  }

  getFactura() {
    this.facturaXML = new XMLFactura();
    this.facturaXMLSign = new XMLFacturaXades();
    //this.soapClientSRI = new SOAPClientSRI();

    var xmlFacturaInfoTributaria = new XMLFacturaInfoTributaria();
    xmlFacturaInfoTributaria.ambiente = SRITipoAmbiente.Pruebas;
    xmlFacturaInfoTributaria.tipoEmision = SRIEmision.EmisionNormal;
    xmlFacturaInfoTributaria.razonSocial = "SERVICIO DE RENTAS INTERNAS";
    xmlFacturaInfoTributaria.nombreComercial = "LE HACE BIEN AL PAIS";
    xmlFacturaInfoTributaria.ruc = "1760013210001";
    //xmlFacturaInfoTributaria.claveAcceso = "0503201201176001321000110010030009900641234567814";
    xmlFacturaInfoTributaria.codDoc = SRITipoComprobante.Factura;
    xmlFacturaInfoTributaria.estab = "001";
    xmlFacturaInfoTributaria.ptoEmi = "003";
    //xmlFacturaInfoTributaria.secuencial = "000990064";
    xmlFacturaInfoTributaria.dirMatriz = "AMAZONAS Y ROCA";
    this.facturaXML.setInfoTributaria(xmlFacturaInfoTributaria);

    var xmlFacturaInfoFactura = new XMLFacturaInfoFactura();
    xmlFacturaInfoFactura.fechaEmision = moment().format("DD/MM/YYYY");
    xmlFacturaInfoFactura.dirEstablecimiento = "SALINAS Y SANTIAGO";
    xmlFacturaInfoFactura.contribuyenteEspecial = "12345";
    xmlFacturaInfoFactura.obligadoContabilidad = "SI";
    xmlFacturaInfoFactura.tipoIdentificacionComprador = SRITipoIndentificacion.Cedula;
    xmlFacturaInfoFactura.guiaRemision = "001-001-000000001";
    xmlFacturaInfoFactura.razonSocialComprador = "EGUIGUREN PENARRETA GABRIEL FERNANDO";
    xmlFacturaInfoFactura.identificacionComprador = "1103029144";
    xmlFacturaInfoFactura.direccionComprador = "salinas y santiago";
    xmlFacturaInfoFactura.totalSinImpuestos = 100.0;
    xmlFacturaInfoFactura.totalDescuento = 0.0;

    var xmlFacturaTotalImpuesto = new XMLFacturaTotalImpuesto();
    xmlFacturaTotalImpuesto.codigo = "2";
    xmlFacturaTotalImpuesto.codigoPorcentaje = "2";
    xmlFacturaTotalImpuesto.descuentoAdicional = 0.0;
    xmlFacturaTotalImpuesto.baseImponible = 100.0;
    xmlFacturaTotalImpuesto.valor = 12.0;
    xmlFacturaInfoFactura.addTotalConImpuestos(xmlFacturaTotalImpuesto);
  
    xmlFacturaInfoFactura.propina = 0.0;
    xmlFacturaInfoFactura.importeTotal = 112.0;
    xmlFacturaInfoFactura.moneda = SRIMoneda.DOLAR;

    var xmlFacturaPago = new XMLFacturaPago();
    xmlFacturaPago.formaPago = "01";
    xmlFacturaPago.total = 112.0;
    xmlFacturaPago.plazo = "30";
    xmlFacturaPago.unidadTiempo = "dias";
    xmlFacturaInfoFactura.addPagos(xmlFacturaPago);

    xmlFacturaInfoFactura.valorRetIva = 0;
    xmlFacturaInfoFactura.valorRetRenta = 0;

    this.facturaXML.setInfoFactura(xmlFacturaInfoFactura);

    var xmlFacturaDetalle = new XMLFacturaDetalle();
    xmlFacturaDetalle.codigoPrincipal = "001";
    xmlFacturaDetalle.codigoAuxiliar = "001";
    xmlFacturaDetalle.descripcion = "SILLA DE MADERA";
    xmlFacturaDetalle.cantidad = 1.0;
    xmlFacturaDetalle.precioUnitario = 100.0;
    xmlFacturaDetalle.descuento = 0.0;
    xmlFacturaDetalle.subtotal = 100.0;

    var xmlFacturaDetalleAdicional = new XMLFacturaDetalleAdicional();
    xmlFacturaDetalleAdicional.nombre = "Test";
    xmlFacturaDetalleAdicional.valor = "Valor";
    xmlFacturaDetalle.addDetallesAdicional(xmlFacturaDetalleAdicional);

    var xmlFacturaDetalleImpuesto = new XMLFacturaDetalleImpuesto();
    xmlFacturaDetalleImpuesto.codigo = "2";
    xmlFacturaDetalleImpuesto.codigoPorcentaje = "2";
    xmlFacturaDetalleImpuesto.tarifa = 12.0;
    xmlFacturaDetalleImpuesto.baseImponible = 100.0;
    xmlFacturaDetalleImpuesto.valor = 12.0;
    xmlFacturaDetalle.addImpuestos(xmlFacturaDetalleImpuesto);

    this.facturaXML.addDetalles(xmlFacturaDetalle);

    var xmlFacturaInfoAdicional = new XMLFacturaInfoAdicional();
    xmlFacturaInfoAdicional.nombre = "Dirección";
    xmlFacturaInfoAdicional.valor = "LOS PERALES Y AV. ELOY ALFARO";
    this.facturaXML.addInfoAdicional(xmlFacturaInfoAdicional);
    xmlFacturaInfoAdicional.nombre = "Teléfono";
    xmlFacturaInfoAdicional.valor = "2123123";
    this.facturaXML.addInfoAdicional(xmlFacturaInfoAdicional);
    xmlFacturaInfoAdicional.nombre = "Email";
    xmlFacturaInfoAdicional.valor = "gfeguiguren@sri.gob.ec";
    this.facturaXML.addInfoAdicional(xmlFacturaInfoAdicional);

    this.facturaXML.setNroComprobante(17);
    this.facturaXML.setSerie(0);

    this.facturaXML.setClaveAcceso();

    this.facturaXMLSign.setXMLFactura(this.facturaXML);
  }

  render() {
    
    return (
      <div className="App">
        <textarea value={this.state.xmlString} cols={80} rows={14} >
        </textarea>
        <br /><br />
        <textarea value={this.state.xmlSignedString} cols={80} rows={18}>
        </textarea>
        <br /><br />
        <textarea value={this.state.respuestaSRI} cols={80} rows={10}>
        </textarea>
        <br /><br />
        <button onClick={this.signRandom.bind(this)}>Sign Random</button>
        <button onClick={this.signWithCert.bind(this)}>Sign Using Certificate</button>
      </div>
    );
  }

  signRandom() {
    this.facturaXMLSign.fetchSignRandom()
    .then(() => {
      this.setState({
        xmlString: this.facturaXML.toXMLString(),
        xmlSignedString: this.facturaXMLSign.xmlFacturaFirmadaString
      });
    });
  }

  signWithCert() {
    this.facturaXMLSign.fetchSign()
    .then(() => {
      const facturaFirmada = this.facturaXMLSign.xmlFacturaFirmadaString;
      let respuestaSRI = '';
      this.facturaXMLSign.fetchRecepcion()
      .then((response) => {
        respuestaSRI += JSON.stringify(response.respuesta) + "\n\n";
        this.facturaXMLSign.fetchAutorizacion()
        .then((response) => {
          respuestaSRI += JSON.stringify(response.respuesta);
          this.setState({
            xmlString: this.facturaXML.toXMLString(),
            xmlSignedString: facturaFirmada,
            respuestaSRI: respuestaSRI
          });
        });
      });
    });
  }
}

export default App;
