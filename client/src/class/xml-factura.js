const axios = require('axios');
const moment = require('moment');

const API_SIGN_URL = '/api/sign';
const API_SIGN_RANDOM_URL = '/api/signRandom';
const API_SRI_RECEPCION_URL = '/api/recepcion';
const API_SRI_AUTORIZACION_URL = '/api/autorizacion';

function emptyArray(array) {
    while (array.length > 0) {
        array.pop();
    }
}

export class XMLFacturaDetalleAdicional {
    nombre;
    valor;

    toXMLString() {
        return `<detAdicional nombre="${this.nombre}" valor="${this.valor}" />`;
    }
}

export class XMLFacturaDetalleImpuesto {
    codigo;
    codigoPorcentaje;
    tarifa;
    baseImponible;
    valor;

    toXMLString() {
        return	`<impuesto>` + 
                `<codigo>${this.codigo}</codigo>` + 
                `<codigoPorcentaje>${this.codigoPorcentaje}</codigoPorcentaje>` + 
                `<tarifa>${this.tarifa}</tarifa>` + 
                `<baseImponible>${this.baseImponible}</baseImponible>` + 
                `<valor>${this.valor}</valor>` + 
                `</impuesto>`;
    }
}

export class XMLFacturaDetalle {
    codigoPrincipal;
    codigoAuxiliar;
    descripcion;
    cantidad;
    precioUnitario;
    descuento;
    subtotal;
    detallesAdicional;
    impuestos;

    constructor () {
        this.detallesAdicional = [];
        this.impuestos = [];
    }

    addDetallesAdicional(detallesAdicional) {
        this.detallesAdicional.push(detallesAdicional);
    }

    addImpuestos(impuestos) {
        this.impuestos.push(impuestos);
    }

    emptyDetallesAdicional() {
        emptyArray(this.detallesAdicional);
    }

    emptyImpuestos() {
        emptyArray(this.impuestos);
    }

    toXMLString() {
        var xmlDetallesAdicional = "";
        var xmlImpuestos = "";

        this.detallesAdicional.forEach(function(element) {
            xmlDetallesAdicional += element.toXMLString();
        });
        xmlDetallesAdicional = (this.detallesAdicional.length > 0) ? `<detallesAdicionales>${xmlDetallesAdicional}</detallesAdicionales>` : "";
        this.impuestos.forEach(function(element) {
            xmlImpuestos += element.toXMLString();
        });
        xmlImpuestos = (this.impuestos.length > 0) ? `<impuestos>${xmlImpuestos}</impuestos>` : "";
        return	`<detalle>` +
                `<codigoPrincipal>${this.codigoPrincipal}</codigoPrincipal>` +
                `<codigoAuxiliar>${this.codigoAuxiliar}</codigoAuxiliar>` +
                `<descripcion>${this.descripcion}</descripcion>` +
                `<cantidad>${this.cantidad}</cantidad>` +
                `<precioUnitario>${this.precioUnitario}</precioUnitario>` +
                `<descuento>${this.descuento}</descuento>` +
                `<precioTotalSinImpuesto>${this.subtotal}</precioTotalSinImpuesto>` +
                xmlDetallesAdicional +
                xmlImpuestos +
                `</detalle>`;
    }
}

export class XMLFacturaInfoTributaria {
    ambiente;
    tipoEmision;
    razonSocial;
    nombreComercial;
    ruc;
    claveAcceso;
    codDoc;
    estab;
    ptoEmi;
    secuencial;
    dirMatriz;

    toXMLString() {
        return	`<ambiente>${this.ambiente}</ambiente>` +
                `<tipoEmision>${this.tipoEmision}</tipoEmision>` +
                `<razonSocial>${this.razonSocial}</razonSocial>` +
                `<nombreComercial>${this.nombreComercial}</nombreComercial>` +
                `<ruc>${this.ruc}</ruc>` +
                `<claveAcceso>${this.claveAcceso}</claveAcceso>` +
                `<codDoc>${this.codDoc}</codDoc>` +
                `<estab>${this.estab}</estab>` +
                `<ptoEmi>${this.ptoEmi}</ptoEmi>` +
                `<secuencial>${this.secuencial}</secuencial>` +
                `<dirMatriz>${this.dirMatriz}</dirMatriz>`;
    }
}

export class XMLFacturaPago {
    formaPago;
    total;
    plazo;
    unidadTiempo;

    toXMLString() {
        return	`<pago>` + 
                `<formaPago>${this.formaPago}</formaPago>` + 
                `<total>${this.total}</total>` + 
                `<plazo>${this.plazo}</plazo>` + 
                `<unidadTiempo>${this.unidadTiempo}</unidadTiempo>` + 
                `</pago>`;
    }
}

export class XMLFacturaTotalImpuesto {
    codigo;
    codigoPorcentaje;
    descuentoAdicional;
    baseImponible;
    valor;

    toXMLString() {
        return	`<totalImpuesto>` + 
                `<codigo>${this.codigo}</codigo>` + 
                `<codigoPorcentaje>${this.codigoPorcentaje}</codigoPorcentaje>` + 
                `<descuentoAdicional>${this.descuentoAdicional}</descuentoAdicional>` + 
                `<baseImponible>${this.baseImponible}</baseImponible>` + 
                `<valor>${this.valor}</valor>` + 
                `</totalImpuesto>`;
    }
}

export class XMLFacturaInfoFactura {
    fechaEmision;
    dirEstablecimiento;
    contribuyenteEspecial;
    obligadoContabilidad;
    tipoIdentificacionComprador;
    guiaRemision;
    razonSocialComprador;
    identificacionComprador;
    direccionComprador;
    totalSinImpuestos;
    totalDescuento;
    totalConImpuestos;
    propina;
    importeTotal;
    moneda;
    pagos;
    valorRetIva;
    valorRetRenta;

    constructor() {
        this.totalConImpuestos = [];
        this.pagos = [];
    }

    addTotalConImpuestos(totalConImpuestos) {
        this.totalConImpuestos.push(totalConImpuestos);
    }

    addPagos(pago) {
        this.pagos.push(pago);
    }

    emptyTotalConImpuestos() {
        emptyArray(this.totalConImpuestos);
    }

    emptyPagos() {
        emptyArray(this.pagos);
    }

    toXMLString() {
        var xmlTotalConImpuestos = "";
        var xmlPagos = "";

        this.totalConImpuestos.forEach(function(element) {
            xmlTotalConImpuestos += element.toXMLString();
        });
        xmlTotalConImpuestos = (this.totalConImpuestos.length > 0) ? `<totalConImpuestos>${xmlTotalConImpuestos}</totalConImpuestos>` : "";
        this.pagos.forEach(function(element) {
            xmlPagos += element.toXMLString();
        });
        xmlPagos = (this.pagos.length > 0) ? `<pagos>${xmlPagos}</pagos>` : "";

        return	`<fechaEmision>${this.fechaEmision}</fechaEmision>` +
                `<dirEstablecimiento>${this.dirEstablecimiento}</dirEstablecimiento>` +
                `<contribuyenteEspecial>${this.contribuyenteEspecial}</contribuyenteEspecial>` +
                `<obligadoContabilidad>${this.obligadoContabilidad}</obligadoContabilidad>` +
                `<tipoIdentificacionComprador>${this.tipoIdentificacionComprador}</tipoIdentificacionComprador>` +
                `<guiaRemision>${this.guiaRemision}</guiaRemision>` +
                `<razonSocialComprador>${this.razonSocialComprador}</razonSocialComprador>` +
                `<identificacionComprador>${this.identificacionComprador}</identificacionComprador>` +
                `<direccionComprador>${this.direccionComprador}</direccionComprador>` +
                `<totalSinImpuestos>${this.totalSinImpuestos}</totalSinImpuestos>` +
                `<totalDescuento>${this.totalDescuento}</totalDescuento>` +
                xmlTotalConImpuestos +
                `<propina>${this.propina}</propina>` +
                `<importeTotal>${this.importeTotal}</importeTotal>` +
                `<moneda>${this.moneda}</moneda>` +
                xmlPagos +
                `<valorRetIva>${this.valorRetIva}</valorRetIva>` +
                `<valorRetRenta>${this.valorRetRenta}</valorRetRenta>`;
    }
}

export class XMLFacturaInfoAdicional {
    nombre;
    valor;

    toXMLString() {
        return `<campoAdicional nombre="${this.nombre}">${this.valor}</campoAdicional>`;
    }
}

export class XMLFactura {
    nroComprobante;
    serie;
    infoTributaria;
    infoFactura;
    detalles;
    infoAdicional;

    constructor() {
        this.infoTributaria = new XMLFacturaInfoTributaria();
        this.infoFactura = new XMLFacturaInfoFactura();
        this.detalles = [];
        this.infoAdicional = [];
    }

    setNroComprobante(nroComprobante) {
        this.nroComprobante = this.getPadZeros(nroComprobante, 9);
        this.infoTributaria.secuencial = this.getPadZeros(nroComprobante, 9);
    }

    setSerie(serie) {
        this.serie = this.getPadZeros(serie, 6);
    }

    setClaveAcceso() {
        this.infoTributaria.claveAcceso = this.getClaveAcceso();
    }

    setInfoTributaria(infoTributaria) {
        this.infoTributaria.ambiente = infoTributaria.ambiente;
        this.infoTributaria.tipoEmision = infoTributaria.tipoEmision;
        this.infoTributaria.razonSocial = infoTributaria.razonSocial;
        this.infoTributaria.nombreComercial = infoTributaria.nombreComercial;
        this.infoTributaria.ruc = infoTributaria.ruc;
        this.infoTributaria.claveAcceso = infoTributaria.claveAcceso;
        this.infoTributaria.codDoc = infoTributaria.codDoc;
        this.infoTributaria.estab = infoTributaria.estab;
        this.infoTributaria.ptoEmi = infoTributaria.ptoEmi;
        this.infoTributaria.secuencial = infoTributaria.secuencial;
        this.infoTributaria.dirMatriz = infoTributaria.dirMatriz;
    }

    setInfoFactura(infoFactura) {
        this.infoFactura.fechaEmision = infoFactura.fechaEmision;
        this.infoFactura.dirEstablecimiento = infoFactura.dirEstablecimiento;
        this.infoFactura.contribuyenteEspecial = infoFactura.contribuyenteEspecial;
        this.infoFactura.obligadoContabilidad = infoFactura.obligadoContabilidad;
        this.infoFactura.tipoIdentificacionComprador = infoFactura.tipoIdentificacionComprador;
        this.infoFactura.guiaRemision = infoFactura.guiaRemision;
        this.infoFactura.razonSocialComprador = infoFactura.razonSocialComprador;
        this.infoFactura.identificacionComprador = infoFactura.identificacionComprador;
        this.infoFactura.direccionComprador = infoFactura.direccionComprador;
        this.infoFactura.totalSinImpuestos = infoFactura.totalSinImpuestos;
        this.infoFactura.totalDescuento = infoFactura.totalDescuento;
        this.infoFactura.totalConImpuestos = infoFactura.totalConImpuestos;
        this.infoFactura.propina = infoFactura.propina;
        this.infoFactura.importeTotal = infoFactura.importeTotal;
        this.infoFactura.moneda = infoFactura.moneda;
        this.infoFactura.pagos = infoFactura.pagos;
        this.infoFactura.valorRetIva = infoFactura.valorRetIva;
        this.infoFactura.valorRetRenta = infoFactura.valorRetRenta;
    }

    addDetalles(detalle) {
        this.detalles.push(detalle);
    }

    addInfoAdicional(infoAdicional) {
        this.infoAdicional.push(infoAdicional);
    }

    emptyDetalles() {
        emptyArray(this.detalles);
    }

    emptyInfoAdicional() {
        emptyArray(this.infoAdicional);
    }

    setDetalles(detalles) {
        let self = this;
        this.emptyDetalles();
        detalles.forEach(function(element) {
            self.detalles.push(element);
        });
    }

    setInfoAdicional(infoAdicional) {
        let self = this;
        this.emptyInfoAdicional();
        infoAdicional.forEach(function(element) {
            self.infoAdicional.push(element);
        });
    }

    //Utils Methods
    getDateFormatDDMMAAAA(date) {
        let newdate = moment(date,'DD/MM/YYYY');
        return newdate.format('DDMMYYYY');
    }

    getPadZeros(number, length) {
        return ('0000000000' + number).substr(-length);
    }

    getCodigoNumerico() {
        const min = 10000000;
        const max = 99999999;
        return (Math.floor(Math.random() * (max - min + 1)) + min).toString();
    }

    getReverseString(str) {
        return str.split("").reverse().join("");
    }

    getModulo11(claveAcceso) {
        let reversedString = this.getReverseString(claveAcceso);
        let pivot = 2;
        let acum = 0;
        for (let i = 0; i < reversedString.length; i++) {
            acum += parseInt(reversedString[i], 10) * pivot;
            pivot = (pivot === 8) ? 2 : ++pivot;
        }
        const digitoVerificador = 11 - (acum % 11);
        return ((digitoVerificador === 11) ? 0 : (digitoVerificador === 10) ? 1 : digitoVerificador).toString();
    }

    getClaveAcceso() {
        //Generando Clave de Acceso
        let claveAcceso = "";
        //1 - Fecha de Emision - ddmmaaaa - 8 Caracteres
        claveAcceso += this.getDateFormatDDMMAAAA(this.infoFactura.fechaEmision);
        //2 - Tipo de Comprobante - Tabla 3 - 2 Caracteres
        claveAcceso += this.infoTributaria.codDoc;
        //3 - Numero de RUC - 1234567890001 - 13 Caracteres
        claveAcceso += this.infoTributaria.ruc;
        //4 - Tipo de Ambiente - Tabla 4 - 1
        claveAcceso += this.infoTributaria.ambiente
        //5 - Serie - 001001 - 6 Caracteres
        claveAcceso += this.getPadZeros(this.serie, 6);
        //6 - Numero del Comprobante - 000000001 - 9 Caracteres
        claveAcceso += this.getPadZeros(this.nroComprobante, 9);
        //7 - Codigo Numerico - Numerico - 8 Caracteres
        claveAcceso += this.getCodigoNumerico();
        //8 - Tipo de Emision - Tabla 2 - 1 Caracter
        claveAcceso += this.infoTributaria.tipoEmision;
        //9 - Digito Verificador (Modulo 11) - Numerico - 1 Caracter
        claveAcceso += this.getModulo11(claveAcceso);
        return claveAcceso;
    }

    toXMLString() {
        var xmlDetalles = "";
        var xmlInfoAdicional = "";

        this.detalles.forEach(function(element) {
            xmlDetalles += element.toXMLString();
        });
        this.infoAdicional.forEach(function(element) {
            xmlInfoAdicional += element.toXMLString();
        });
        return	`<?xml version="1.0" encoding="UTF-8" ?>` +
                `<factura id="comprobante" version="1.0.0">` +
                `<infoTributaria>${this.infoTributaria.toXMLString()}</infoTributaria>` +
                `<infoFactura>${this.infoFactura.toXMLString()}</infoFactura>` +
                `<detalles>${xmlDetalles}</detalles>` +
                `<infoAdicional>${xmlInfoAdicional}</infoAdicional>` +
                `</factura>`;
    }
}

export class XMLFacturaXades {
    xmlFactura;
    xmlFacturaFirmadaString;
    
    constructor() {
        this.xmlFactura = new XMLFactura();
        this.xmlFacturaFirmadaString = "";
    }

    setXMLFactura(xmlFactura) {
        this.xmlFactura.setNroComprobante(parseInt(xmlFactura.nroComprobante, 10));
        this.xmlFactura.setSerie(parseInt(xmlFactura.serie, 10));
        this.xmlFactura.setInfoTributaria(xmlFactura.infoTributaria);
        this.xmlFactura.setInfoFactura(xmlFactura.infoFactura);
        this.xmlFactura.setDetalles(xmlFactura.detalles);
        this.xmlFactura.setInfoAdicional(xmlFactura.infoAdicional);
    }
    
    async fetchSignExtended(url) {
        // await response of fetch call
        let response = await axios.post(url, JSON.stringify({
            xml: this.xmlFactura.toXMLString()
        }),
        {
            headers: {
                'Content-Type':'application/json'
            }
        });
        if (response.status === 200) {
            this.xmlFacturaFirmadaString = response.data.xmlSigned;
        }
    }

    async fetchSignRandom() {
        await this.fetchSignExtended(API_SIGN_RANDOM_URL);
    }

    async fetchSign() {
        await this.fetchSignExtended(API_SIGN_URL);
    }

    async fetchSRIExtended(url, data) {
        let response = await axios.post(url, data,
        {
            headers: {
                'Content-Type':'application/json'
            }
        });
        if (response.status === 200) {
            return response.data;
        }
    }

    async fetchRecepcion() {
        const data = JSON.stringify({
            xml: this.xmlFacturaFirmadaString
        });
        return await this.fetchSRIExtended(API_SRI_RECEPCION_URL, data);
    }

    async fetchAutorizacion() {
        const data = JSON.stringify({
            claveAccesoComprobante: this.xmlFactura.infoTributaria.claveAcceso
        });
        return await this.fetchSRIExtended(API_SRI_AUTORIZACION_URL, data);
    }

    toXMLString() {
        return this.xmlFacturaFirmadaString;
    }
}
