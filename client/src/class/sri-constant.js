export class SRIURL {
    static URL = "";
    static URLTest = "";
}

export class SRIEmision {
    static EmisionNormal = "1";
}

export class SRITipoComprobante {
    static Factura = "01";
    static NotaCredito = "04";
    static NotaDebito = "05";
    static GuiaRemision = "06";
    static ComprobanteRetencion = "07";
}

export class SRITipoAmbiente {
    static Pruebas = "1";
    static Produccion = "2";
}

export class SRITipoIndentificacion {
    static RUC = "04";
    static Cedula = "05";
    static Pasaporte = "06";
    static ConsumidorFinal = "07";
    static IdentificacionExterior = "08";
    static Placa = "09";
}

export class SRIMoneda {
    static DOLAR = "DOLAR";
}

//class SRI