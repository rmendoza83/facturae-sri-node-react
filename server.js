const express = require('express');
const bodyParser = require('body-parser');

var XadesClass = require('./class/xades.js');
var SoapSRI = require('./class/soap-sri.js');

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/api/signRandom', (request, response) => {
  xmlToSign = request.body.xml;
  var xades = new XadesClass();
  xades.signWithRandom(xmlToSign)
    .then(() => {
      response.send({
        xmlSigned: xades.signedXML
      });
      console.log('Signed Random!!!');
    });
});

app.post('/api/sign', (request, response) => {
  xmlToSign = request.body.xml;
  var xades = new XadesClass();
  xades.signWithCert(xmlToSign)
    .then(() => {
      response.send({
        xmlSigned: xades.signedXML
      });
      console.log('Signed!!!');
    });
});

app.post('/api/recepcion', (request, response) => {
  var soapSRI = new SoapSRI(false);
  soapSRI.validarComprobante(request.body.xml)
    .then((respuestaSolicitud) => {
      response.send({
        respuesta: respuestaSolicitud
      });
      console.log('Validar Comprobante!!!');
    });
});

app.post('/api/autorizacion', (request, response) => {
  var soapSRI = new SoapSRI(false);
  soapSRI.autorizacionComprobante(request.body.claveAccesoComprobante)
    .then((respuestaSolicitud) => {
      response.send({
        respuesta: respuestaSolicitud
      });
      console.log('Autorizar Comprobante!!!');
    });
});

const port = process.env.PORT || 5000;
app.listen(port);

console.log('App is listening on port ' + port);
